
var App = function() {

    var isMobile;
    var isTablet;
    var isDesktop;
    var isRTL = false;

    var initBootstrap = function()
    {
        $('[data-toggle="tooltip"]').tooltip()
    }

    var toggleResizeBootstrap = function()
    {
        if ( isDesktop != true )
        {
            $('[data-toggle="tooltip"]').tooltip('destroy');
        }
        else
        {
            $('[data-toggle="tooltip"]').tooltip()
        }
    }

    var toggleSidebar = function()
    {
        if ( isDesktop == true )
        {
            $('.sidebar').show();
        }
        else
        {
            $('.sidebar').hide();
        }
    }

    var initResize = function()
    {
        $(window).resize(function() {
            initDetectResize();
        });
    }

    var initDetectResize = function()
    {
        var innerW = window.innerWidth;
        isDesktop = false;
        isTablet = false;
        isMobile = false;
        if(innerW >= 768 && innerW <= 979){
            isTablet = true;
        }else if(innerW > 979){
            isDesktop = true;
        }else{
            isMobile = true;
        }

        toggleResizeBootstrap();
        toggleSidebar();

        return innerW;
    }


    var initSlimScroll = function(el) {
        if (!$().slimScroll) {
            return;
        }

        $(el).each(function() {
            if ($(this).attr("data-initialized")) {
                return; // exit
            }

            var height;

            if ($(this).attr("data-height")) {
                height = $(this).attr("data-height");
            } else {
                height = $(this).css('height');
            }

            $(this).slimScroll({
                allowPageScroll: true, // allow page scroll when the element scroll is ended
                size: '7px',
                color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
                wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
                position: isRTL ? 'left' : 'right',
                height: height,
                alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                disableFadeOut: true
            });

            $(this).attr("data-initialized", "1");
        });
    };

    return {

        init: function()
        {
            initBootstrap();
            initResize();
            initSlimScroll('.scroller');
        },
        toggleSidebar: function()
        {
            $(".sidebar").toggleClass('active');
            $(".sidebar").toggle();

            if ( $(".sidebar").hasClass('active') )
            {
                $(".toggle-sidebar").html('<i class="ion-arrow-left-b"></i>');
            }
            else
            {
                $(".toggle-sidebar").html('<i class="ion-arrow-right-b"></i>');
            }
        }
    };

}();


$(function () {
    App.init();
})
